#include <iostream>

struct Node {
    int value;
    Node* next;
    Node* prev;
};

int main() {
    int T, N, n;
    std::cin >> T;
    for (size_t t = 0; t < T; ++t) {
        unsigned long long sum = 0;
        std::cin >> N;
        std::cin >> n;
        Node* first = new Node();
        first->value = n;
        first->next = nullptr;
        first->prev = nullptr;
        Node* median = first;
        sum += n;
        bool odd = true;
        int left = 0, right = 0;
        for (size_t i = 1; i < N; ++i) {
            std::cin >> n;
            Node* node = new Node();
            Node* curr = median;
            node->value = n;
            if (node->value > median->value) {
                while(node->value > curr->value) {
                    if (curr->next == nullptr) {
                        curr->next = node;
                        node->prev = curr;
                        node->next = nullptr;
                        break;
                    } else {
                        curr = curr->next;
                        if (node->value <= curr->value) {
                            curr->prev->next = node;
                            node->prev = curr->prev;
                            node->next = curr;
                            curr->prev = node;
                            break;
                        } else {
                            continue;
                        }
                    }
                }
                right++;
            } else if (node->value == median->value) {
                if (curr->prev == nullptr) {
                    node->prev = nullptr;
                    node->next = curr;
                    curr->prev = node;
                } else {
                    curr->prev->next = node;
                    node->prev = curr->prev;
                    node->next = curr;
                    curr->prev = node;
                }
                left++;
            } else {
                while(node->value < curr->value) {
                    if (curr->prev == nullptr) {
                        curr->prev = node;
                        node->next = curr;
                        node->prev = nullptr;
                        break;
                    } else {
                        curr = curr->prev;
                        if (node->value >= curr->value) {
                            curr->next->prev = node;
                            node->next = curr->next;
                            node->prev = curr;
                            curr->next = node;
                            break;
                        } else {
                            continue;
                        }
                    }
                }
                left++;
            }
            odd = !odd;

            if (left > right) {
                //std::cout << "shifting left, l/r: " << left << "/" << right << std::endl;
                median = median->prev;
                left--;
                right++;
                //std::cout << "l/r after shift: " << left << "/" << right << "\n" << std::endl;

            } else if (odd && left < right) {
                //std::cout << "shifting right odd" << std::endl;
                median = median->next;
                left++;
                right--;
            } else if (!odd && left < right - 1) {
                //std::cout << "shifting right even" << std::endl;
                median = median->next;
                left++;
                right--;
            }

            if (odd) {
                sum += median->value;
            } else {
                sum += (median->value + median->next->value) / 2;
            }
            /*
            while (curr->prev != nullptr) {
                curr = curr->prev;
            }
            std::cout << "\nArray: ";
            while(curr != nullptr) {
                std::cout << curr->value << " ";
                curr = curr->next;
            }
            std::cout << "Odd: " << odd;
            std::cout << "\nMedian: " << median->value << std::endl;
            */
        }
        std::cout << sum << std::endl;
    }
    return 0;
}