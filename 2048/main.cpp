#include <iostream>
#include <vector>

std::vector<std::vector<int>> board;

void left() {
    for (size_t i = 0; i < 4; ++i) {
        int k = 3;
        while(k > 0) {
            if (board[i][k-1] == 0 && board[i][k] != 0) {
                board[i][k-1] = board[i][k];
                board[i][k] = 0;
                k = 3;
            } else {
                k--;
            }
        }
        for (size_t j = 0; j < 3; ++j) {
            if (board[i][j] == board[i][j+1]) {
                board[i][j] *= 2;
                board[i][j+1] = 0;
                //j = 3;
            }
        }
        k = 3;
        while(k > 0) {
            if (board[i][k-1] == 0 && board[i][k] != 0) {
                board[i][k-1] = board[i][k];
                board[i][k] = 0;
                k = 3;
            } else {
                k--;
            }
        }
    }
}

void up() {
    for (size_t j = 0; j < 4; ++j) {
        int k = 3;
        while(k > 0) {
            if (board[k-1][j] == 0 && board[k][j] != 0) {
                board[k-1][j] = board[k][j];
                board[k][j] = 0;
                k = 3;
            } else {
                k--;
            }
        }

        for (size_t i = 0; i < 3; ++i) {
            if (board[i][j] == board[i+1][j]) {
                board[i][j] *= 2;
                board[i+1][j] = 0;
                //i = 0;
            }
        }
        k = 3;
        while(k > 0) {
            if (board[k-1][j] == 0 && board[k][j] != 0) {
                board[k-1][j] = board[k][j];
                board[k][j] = 0;
                k = 3;
            } else {
                k--;
            }
        }
    }
}

void down() {
    for (size_t j = 0; j < 4; ++j) {
        int k = 0;
        while(k < 3) {
            if (board[k+1][j] == 0 && board[k][j] != 0) {
                board[k+1][j] = board[k][j];
                board[k][j] = 0;
                k = 0;
            } else {
                k++;
            }
        }
        for (size_t i = 3; i > 0; --i) {
            if (board[i][j] == board[i-1][j]) {
                board[i][j] *= 2;
                board[i-1][j] = 0;
                //i = 3;
            }
        }
        k = 0;
        while(k < 3) {
            if (board[k+1][j] == 0 && board[k][j] != 0) {
                board[k+1][j] = board[k][j];
                board[k][j] = 0;
                k = 0;
            } else {
                k++;
            }
        }
    }
}

void right() {
    for (size_t i = 0; i < 4; ++i) {
        int k = 0;
        while(k < 3) {
            if (board[i][k+1] == 0 && board[i][k] != 0) {
                board[i][k+1] = board[i][k];
                board[i][k] = 0;
                k = 0;
            } else {
                k++;
            }
        }
        for (size_t j = 3; j > 0; --j) {
            if (board[i][j] == board[i][j-1]) {
                board[i][j] *= 2;
                board[i][j-1] = 0;
                //j = 0;
            }
        }
        k = 0;
        while(k < 3) {
            if (board[i][k+1] == 0 && board[i][k] != 0) {
                board[i][k+1] = board[i][k];
                board[i][k] = 0;
                k = 0;
            } else {
                k++;
            }
        }
    }
}

void print() {
    for (size_t i = 0; i < 4; ++i) {
        for (size_t j = 0; j < 4; ++j) {
            std::cout << board[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

int main() {
    int n;
    for (size_t i = 0; i < 4; ++i) {
        std::vector<int> row;
        for (size_t j = 0; j < 4; ++j) {
            std::cin >> n;
            row.push_back(n);
        }
        board.push_back(row);
    }
    std::cin >> n;
    switch (n)
    {
    case 0:
        left();
        break;
    case 1:
        up();
        break;
    case 2:
        right();
        break;
    case 3:
        down();
        break;
    default:
        break;
    }
    print();
    return 0;
}