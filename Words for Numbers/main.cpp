#include <iostream>
#include <string>
#include <map>

std::map<std::string,std::string> nums = {
    {"0", "zero"},
    {"1", "one"},
    {"2", "two"},
    {"3", "three"},
    {"4", "four"},
    {"5", "five"},
    {"6", "six"},
    {"7", "seven"},
    {"8", "eight"},
    {"9", "nine"},
    {"10", "ten"},
    {"11", "eleven"},
    {"12", "twelve"},
    {"13", "thirteen"},
    {"14", "fourteen"},
    {"15", "fifteen"},
    {"16", "sixteen"},
    {"17", "seventeen"},
    {"18", "eighteen"},
    {"19", "nineteen"},
    {"20", "twenty"},
    {"30", "thirty"},
    {"40", "fourty"},
    {"50", "fifty"},
    {"60", "sixty"},
    {"70", "seventy"},
    {"80", "eighty"},
    {"90", "ninety"}
};

int main() {
    std::string line, output, s;
    std::map<std::string,std::string>::iterator it;
    while (std::getline(std::cin, line)) {
        output = "";
        for (size_t i = 0; i < line.length(); ++i) {
            s = "";
            if (line[i] >= 48 && line[i] <= 57) {
                if (i < line.length() - 1) {
                    if (line[i+1] >= 48 && line[i+1] <= 57) {
                        s += line[i];
                        s += line[i+1];
                        it = nums.find(s);
                        if (it == nums.end()) {
                            s = "";
                            s += line[i];
                            s += '0';
                            output += nums[s];
                            output += "-";
                            s = "";
                            s += line[i+1];
                            output.append(nums[s]);
                        } else {
                            output += nums[s];
                        }
                        if (i == 0) {
                            output[0] = output[0] - 32;
                        }
                        ++i;
                    } else {
                        s += line[i];
                        output += nums[s];
                        if (i == 0) {
                            output[0] = output[0] - 32;
                        }
                    }
                }
            } else {
                output += line[i];
            }
        }
        std::cout << output << std::endl;
    }
    return 0;
}