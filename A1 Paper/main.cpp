#include <iostream>
#include <cmath>
#include <iomanip>

std::pair<double,double> a2 = {std::pow(2.0, (-5.0/4.0)), std::pow(2.0, (-3.0/4.0))};


int main() {
    int n, m;
    std::cin >> n;
    double sum = 0.0;
    double tape = 0.0;

    for (size_t i = 1; i < n; ++i) {
        std::cin >> m;
        if (i > 1) {
            a2 = {a2.second / 2, a2.first};
        }
        for (size_t j = 1; j <= m; ++j) {
            sum += 1 / std::pow(2, i);
            if (sum < 1.0) {
                if (j % 2 == 0) {
                    if (j % 4 == 0) {
                        tape += a2.second * 2;
                    } else {
                        tape += a2.first + a2.second;
                    }
                } else if (j % 3 == 0) {
                    tape += a2.first;
                } else {
                    tape += a2.second;
                }
            }
            else {
                break;
            }
        }
        if (sum >= 1.0) {
            break;
        }
    }
    if (sum == 1.0) {
        std::cout << std::setprecision(6) << tape;
    } else {
        std::cout << "impossible";
    }


    return 0;
}