#include <iostream>
#include <map>
#include <string>
#include <vector>

// Not working

std::map<int,std::string> doubles = {
    {4+4, "4 + 4"},
    {4-4, "4 - 4"},
    {4*4, "4 * 4"},
    {4/4, "4 / 4"}
};

std::map<int,std::string> triples = {
    {4+4+4, "4 + 4 + 4"},
    {4+4-4, "4 + 4 - 4"},
    {4+4*4, "4 + 4 * 4"},
    {4+4/4, "4 + 4 / 4"},
    {4-4*4, "4 - 4 * 4"},
    {4-4/4, "4 - 4 / 4"},
    {4*4*4, "4 * 4 * 4"},
    {4*4/4, "4 * 4 / 4"},
    {4/4/4, "4 / 4 / 4"}
};

std::vector<std::string> answers;
std::vector<int> numbers;

int main() {
    int m, n;
    std::cin >> m;
    for (size_t i = 0; i < m; ++i) {
        std::cin >> n;
        std::cin.ignore();
        numbers.push_back(n);
    }
    for (size_t i = 0; i < m; ++i) {
        n = numbers[i];
        std::string s = "0";
        std::map<int,std::string>::iterator it, it2;
        for (it = doubles.begin(); it != doubles.end(); it++) {
            if (s != "0") {
                break;
            }
            for (it2 = doubles.begin(); it2 != doubles.end(); it2++) {
                if (s != "0") {
                    break;
                }
                if (it->first + it2->first == n) {
                    s = it->second + " + " + it2->second;
                } else if (it->first - it2->first == n) {
                    s = it->second + " - " + it2->second;
                } else if (it->first * it2->first == n) {
                    s = it->second + " * " + it2->second;
                } else if (it2->first != 0 && it->first / it2->first == n) {
                    s = it->second + " / " + it2->second;
                } else {
                    continue;
                }
            }
        }
        if (s == "0") {
            for (it = triples.begin(); it != triples.end(); it++) {
                if (s != "0") {
                    break;
                }
                if (4 + it->first == n) {
                    s = "4 + " + it->second;
                } else if (it->first + 4 == n) {
                    s = it->second + " + 4";
                } else if (4 - it->first == n) {
                    s = "4 - " + it->second;
                } else if (it->first - 4 == n) {
                    s = it->second + " - 4";
                } else if (4 * it->first == n) {
                    s = "4 * " + it->second;
                } else if (it->first * 4 == n) {
                    s = it->second + " * 4";
                } else if (it->first != 0 && 4 / it->first == n) {
                    s = "4 / " + it->second;
                } else if (it->first / 4 == n) {
                    s = it->second + " / 4";
                } else {
                    continue;
                }
            }
        }
        if (s == "0") {
            answers.push_back("no solution");
        } else {
            s += " = " + std::to_string(n);
            answers.push_back(s);
        }
    }
    for (auto str: answers) {
        std::cout << str << std::endl;
    }
    return 0;
}